package fr.ib.theo.mediatheque3ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(description="Stockage JPA des dvds")
public class DvdDAO implements IDvdDAO {

	private EntityManager em;
	
	public int getNombreDvd() {
		return em.createQuery("select count(*) from Dvd", Long.class).getSingleResult().intValue();
	}

	@Override
	public void ajouterDvd(Dvd dvd) {
		em.persist(dvd);
	}

	@Override
	public Dvd lire(int id) {
		return em.find(Dvd.class, id);
	}

	public List<Dvd> lireTous() {
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}

	// on utilise la persistence 
	@PersistenceContext(unitName="DvdPU")
	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	
}
