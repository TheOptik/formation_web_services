package fr.ib.theo.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

// stateless -> pas d'attribut, tous les clients utilise le meme objet
// statefull -> plusieurs attributs (derniereInterro)
@Stateful(name="Dvds", description="opéation pour la DVDtheque")
public class Dvdtheque implements InterfaceDvdtheque {

	private LocalTime derniereInterro;
	private IDvdDAO dvdDAO;
	
	public String getInfos() {
		return "Des infos sur la dvdtheque, ouvert de 8h à 18h.\n"+
				"Il y a " + dvdDAO.getNombreDvd() + " DVDs.";
	}
	public boolean ouvertA(LocalTime t) {
		derniereInterro = t;
		return t.isAfter(LocalTime.of(8, 0)) && t.isBefore(LocalTime.of(18, 0));
	}
	public LocalTime getDerniereInterrogation() {
		return derniereInterro;
	}
	
	//@EJB(lookup="ejb:/mediatheque3ejb/DvdDAO!fr.ib.theo.mediatheque3ejb.IDvdDAO")
	@EJB(name="DvdDAO")
	public void setDvdDAO(IDvdDAO dao) {
		this.dvdDAO = dao;
	}
}
