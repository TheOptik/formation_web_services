package fr.ib.theo.mediatheque3ejb;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dvd implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String titre;
	private int annee;
	
	public Dvd() {
		this(null, 0);
	}

	public Dvd(String titre, int annee) {
		this.titre = titre;
		this.annee = annee;
	}

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	@Override
	public String toString() {
		return "Dvd [id=" + id + ", titre=" + titre + ", annee=" + annee + "]";
	}
	
	
}
