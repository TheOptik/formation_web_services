package fr.ib.theo.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

// rend les choses utilisables par les clients en local et à distance
@Remote
public interface IDvdDAO {
	public int getNombreDvd();
	public void ajouterDvd(Dvd dvd);
	public Dvd lire(int id);
	public List<Dvd> lireTous();
}
