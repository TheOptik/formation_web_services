package fr.ib.theo.mediatheques2spring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// http://localhost:9002/health
@RestController
public class HealthController {

	@RequestMapping("/health")
	public String getHealth() {
		return "OK";
	}
}
