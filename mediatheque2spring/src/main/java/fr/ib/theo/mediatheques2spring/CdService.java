package fr.ib.theo.mediatheques2spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CdService {
	private List<Cd> cds = new ArrayList<>();
	
	public CdService() {
		
	}
	
	public int getNombreCd() {
		return cds.size();
	}
	
	public void ajoutCd(Cd cd) {
		cds.add(cd);
	}
	
	public List<Cd> getCds(){
		return cds;
	}

	public void changeTitrePremier(int n, String t) {
		cds.get(n).setTitre(t);	
	}
	
}
