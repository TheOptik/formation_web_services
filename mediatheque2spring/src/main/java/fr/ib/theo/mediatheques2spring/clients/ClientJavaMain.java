package fr.ib.theo.mediatheques2spring.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
// lancer application avant 

public class ClientJavaMain {

	public static void main(String[] args) {
		System.out.println("Client java");
		
		try {
			URL url = new URL("http://localhost:9002/health");
			URLConnection co = url.openConnection();
			InputStream is = co.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String response = br.readLine();
			System.out.println("Health : " + response);
			br.close();
			
		} catch(Exception e) {
			System.err.println("Erreur : " + e);
		}
	}

}
