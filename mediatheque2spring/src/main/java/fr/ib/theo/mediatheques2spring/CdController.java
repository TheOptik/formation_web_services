package fr.ib.theo.mediatheques2spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CdController {
	private CdService cdService;
	
	// transforme en xml (ajout librairie jackson)
	@RequestMapping(path="/cd/nombre", produces=MediaType.APPLICATION_XML_VALUE, method=RequestMethod.GET)
	public int getNombre() {
		return cdService.getNombreCd();
	}
	
	// ajout d'un nouveau cd
	@RequestMapping(path="/cd", method=RequestMethod.POST)
	public void ajoute(@RequestBody Cd cd) {
		cdService.ajoutCd(cd);
	}
	
	// affiche les cds
	@RequestMapping(path="/cd", method=RequestMethod.GET)
	public List<Cd> afficheCds() {
		return cdService.getCds();
	}
	
	// pathVariable -> num est lié à n
	// :\\d+ -> num composé de 1 ou plusieurs chiffres
	@RequestMapping(path="/cd/{num:\\d+}", method=RequestMethod.GET)
	public Cd getElemAvecIndex(@PathVariable("num") int n) {
		return cdService.getCds().get(n);
	}
	
	@RequestMapping(path="/cd/{num:\\d+}/titre", method=RequestMethod.PUT)
	public void changeTitre(@PathVariable("num") int n, @RequestBody String t) {
		cdService.changeTitrePremier(n, t);
	}
	
	@RequestMapping(path="/json/cd", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> afficheCdsJson() {
		return cdService.getCds();
	}
	
	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}
	
}
