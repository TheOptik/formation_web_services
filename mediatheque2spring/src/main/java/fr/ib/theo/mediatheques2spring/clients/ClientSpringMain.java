package fr.ib.theo.mediatheques2spring.clients;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.theo.mediatheques2spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		System.out.println("Client spring");
		
		try {
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : " + health);
	
			Integer nbCds = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Nombre de cds : " + nbCds);
			
			// ajout d'un nouveau cd
			Cd cd1 = new Cd("Abbey Road", "The Beatles", 1966);
			// fonction sans resultat
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);
			
			// recup la liste des cds, getForObject non utilisable dans ce cas
			ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {};
			ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
			List<Cd> cds = cdsEntity.getBody();
			cds.forEach(System.out::println);
			// autre méthode : 
			Cd[] cds2 = rt.getForObject("http://localhost:9002/cd", Cd[].class);
			for(Cd cd: cds2) { System.out.println(cd); }
			
			// recup premier cd avec index
			Cd cd0 = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("premier cd : " + cd0);
			
			// modifier le titre du cd 0
			// String.class -> type du nouveau titre Help
			rt.put("http://localhost:9002/cd/0/titre", "Help!", String.class);
			System.out.println(cds.get(0).getTitre()); // lancer 2 clients pour voir le changement
			
		} catch(Exception e) {
			System.err.println("Erreur : " + e);
		}
		
	}

}
