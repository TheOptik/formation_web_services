package fr.ib.theo.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.theo.mediatheque1cxf.server.InterfaceLivresService;

public class ClientMain {

	public static void main(String[] args) {
		System.out.println("Client livres : ");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(InterfaceLivresService.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		
		InterfaceLivresService livresService = factory.create(InterfaceLivresService.class);
		System.out.println(livresService.getInfos());
		System.out.println("Livre 4 empruntable : " + livresService.estEmpruntable(4));
	
		/*try {
			System.out.println("Livre -3 empruntable : " + livresService.estEmpruntable(-3));
		} catch(Exception e) {
			System.out.println("Exception : " + e.getMessage());
		}*/
		
		System.out.println("Livre 4 à rendre le : " + livresService.getRetour(4));
		System.out.println("Livre recommandé : " + livresService.getLivreDuMois());
		System.out.println("Livre du mois de Mars : " + livresService.getLivresDeLannee()[2]);
	}

}
