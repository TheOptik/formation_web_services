package fr.ib.theo.mediatheque1cxf.server;

import javax.xml.ws.Endpoint;


// http://localhost:9001/livres?wsdl :
// minOccurs = "0" -> renvoie type sinon null

// lancer server puis client 
public class ServerMain {

	public static void main(String[] args) {
		System.out.println("Démarrage du server");
	
		Endpoint epLivres = Endpoint.publish("http://localhost:9001/livres", new LivreService());
	

	}

}
