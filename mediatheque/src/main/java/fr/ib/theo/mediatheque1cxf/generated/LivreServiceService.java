package fr.ib.theo.mediatheque1cxf.generated;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.4.5
 * 2021-12-21T14:53:46.230+01:00
 * Generated source version: 3.4.5
 *
 */
@WebServiceClient(name = "LivreServiceService",
                  wsdlLocation = "http://localhost:9001/livres?wsdl",
                  targetNamespace = "http://server.mediatheque1cxf.theo.ib.fr/")
public class LivreServiceService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://server.mediatheque1cxf.theo.ib.fr/", "LivreServiceService");
    public final static QName LivreServicePort = new QName("http://server.mediatheque1cxf.theo.ib.fr/", "LivreServicePort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:9001/livres?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(LivreServiceService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://localhost:9001/livres?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public LivreServiceService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public LivreServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public LivreServiceService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public LivreServiceService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public LivreServiceService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public LivreServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns InterfaceLivresService
     */
    @WebEndpoint(name = "LivreServicePort")
    public InterfaceLivresService getLivreServicePort() {
        return super.getPort(LivreServicePort, InterfaceLivresService.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns InterfaceLivresService
     */
    @WebEndpoint(name = "LivreServicePort")
    public InterfaceLivresService getLivreServicePort(WebServiceFeature... features) {
        return super.getPort(LivreServicePort, InterfaceLivresService.class, features);
    }

}
