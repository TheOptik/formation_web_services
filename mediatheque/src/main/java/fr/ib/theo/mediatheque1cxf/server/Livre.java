package fr.ib.theo.mediatheque1cxf.server;

import java.beans.Transient;
import java.io.Serializable;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder= {"titre", "auteur", "annee", "image"})
public class Livre implements Serializable{
	private static final long serialVersionUID = 1L;
	private String titre;
	private String auteur;
	private int annee;
	private DataHandler image;
	public Livre() {
		this(null, null, 0);
	}
	public Livre (String t, String a, int ann) {
		titre = t;
		auteur = a;
		annee = ann;
	}
	@Override
	public String toString() {
		return titre+" de "+auteur+" ("+annee+")";
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}	
	public DataHandler getImage() {
		return image;
	}
	public void setImage(DataHandler image) {
		this.image = image;
	}
	@Transient
	public String getConfidentialInternalCode() { 
		return "lkdsjslkdfjlfdj"; 
	}	
	
}
