package fr.ib.theo.mediatheque1cxf.server;
import java.util.Date;
import javax.jws.WebService;

@WebService
public interface InterfaceLivresService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee();
}
