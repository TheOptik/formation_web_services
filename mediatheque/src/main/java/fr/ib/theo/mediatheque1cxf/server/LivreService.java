package fr.ib.theo.mediatheque1cxf.server;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

// code des procédures du service
@WebService
public class LivreService implements InterfaceLivresService {

	public String getInfos() {
		return "Des informations a propos de notre bibliothèque";
	}

	public boolean estEmpruntable(int id) {
		if(id<1){
			throw new IllegalArgumentException("id doit être sup à 1");
		}
		return false;
	}

	public Date getRetour(int id) {
		return Date.from(LocalDate.now().plusDays(10).atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public Livre getLivreDuMois() {
		Livre livre = new Livre("Lala", "Moi", 2000);
		DataSource dataSource = new FileDataSource("image.jpg");
		DataHandler dataHandler = new DataHandler(dataSource);
		livre.setImage(dataHandler);
		return livre;
	}

	public Livre[] getLivresDeLannee() {
		Livre[] livres = new Livre[12];
		for(int i=0; i<12; i++)
			livres[i] = getLivreDuMois();
		return livres;
	}

}
