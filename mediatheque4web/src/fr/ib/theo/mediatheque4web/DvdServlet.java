package fr.ib.theo.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.theo.mediatheque3ejb.InterfaceDvdtheque;

@WebServlet("/dvds")
public class DvdServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private InterfaceDvdtheque dvdtheque;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		
		Writer out = resp.getWriter();
		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDtheque !</h1>");
		out.write("<p>" + dvdtheque.getInfos() + "</p>");
		out.write("<p>Ouvert maintenant ? " + dvdtheque.ouvertA(LocalTime.now()) + "</p>");
		out.write("<p>Derniere utilisation : " + dvdtheque.getDerniereInterrogation() + "</p>");
		out.write("</body></html>");
		out.close();
	}
	
	@EJB(lookup="ejb:/mediatheque3ejb/Dvds!fr.ib.theo.mediatheque3ejb.InterfaceDvdtheque")
	public void setDvdtheque(InterfaceDvdtheque dvd) {
		this.dvdtheque = dvd;
	}
}
