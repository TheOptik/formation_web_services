package fr.ib.theo.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.theo.mediatheque3ejb.InterfaceDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("DVD : client lourd");
		try {
			Context context = new InitialContext();
			InterfaceDvdtheque dvdtheque = (InterfaceDvdtheque) context.lookup(
			"ejb:/mediatheque3ejb/Dvds!fr.ib.theo.mediatheque3ejb.InterfaceDvdtheque");
			
			System.out.println("Informations : " + dvdtheque.getInfos());
			
			System.out.println("Dvdtheque ouvert ? " + (dvdtheque.ouvertA(LocalTime.now()) ? "oui" : "non"));
			
			context.close();
		} catch(Exception e) {
			System.err.println(e);
		}
	}

}
